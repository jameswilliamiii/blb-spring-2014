## Week 1:
- Intro to the lean startup
- Intro to the lean canvas
* Problem
* Customer segments
* Unique value proposition
* Solution
* Unfair advantage
* Revenue stream
* Cost structure
* Key metrics
* Channels

## Week 2:
- Review initial lean canvas from students
- Discuss risk assessment.
- - Product risks
- - Customer risks
- - Market risks
- Discuss design of experiments and validations
- - Customer base / problem
- - Solution / UVP
- - Channels
- - Revenue streams

## Week 3:
- Review risk assessments, validations, and experiments from students
- - Discuss problem interviews


## Week 4:
- Review student problem interviews
- Discuss solution interviews and pivoting (if needed)


## Week 5:
- Review student solution interviews
- Discuss pivoting
- Discuss MVP
- Discuss how to measure results

## Week 6:
- Review student MVP's and how they will measure results
- MVP interview
* Test UVP
* Test pricing
* Test actual solution

## Week 7:
- Review MVP interview results
- Discuss validating customer cycle, product / market fit, and customer feedback / product modifications

## Week 7.5:
- Pitch during ETC open house
- Have a beer to celebrate